//sites referencia para MQTT em android
// https://www.hivemq.com/blog/mqtt-client-library-enyclopedia-paho-android-service/
// https://www.eclipse.org/paho/clients/android/

package br.com.fatec.nodemcu_ligar_led_app;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;


public class MainActivity extends AppCompatActivity {
    //Variáveis
    static String MQTTHOST = "tcp://soldier.cloudmqtt.com:16289"; //apontamento mqtt cloud
    static String USERNAME = "qmsckjxw";
    static String PASSWORD = "7FhpnddzzyqC";
    String topicStr = "LED"; //Topico criado
    MqttAndroidClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Verificação da conexão com o mqtt cloud

        String clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(this.getApplicationContext(), MQTTHOST, clientId);
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(USERNAME);
        options.setPassword(PASSWORD.toCharArray());

        try {
            IMqttToken token = client.connect(options);
            token.setActionCallback(new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Toast.makeText(MainActivity.this,"conectado",Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(MainActivity.this,"não conectado",Toast.LENGTH_LONG).show();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    //Métodos para os botões



    public void ligar(View v){
        String topic = topicStr;
        String message = "L1";
        try {
            client.publish(topic, message.getBytes(),0,false    );
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    public void desligar(View v){
        String topic = topicStr;
        String message = "D1";
        try {
            client.publish(topic, message.getBytes(),0,false    );
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


}
