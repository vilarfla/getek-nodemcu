//nodemcu
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//Configuração de conexão do NODEMCU com Servidor MQTT
const char* ssid = "iPhone de Flavia"; 
const char* password =  "superman100";
const char* mqttServer = "soldier.cloudmqtt.com"; 
const int mqttPort = 16289; 
const char* mqttUser = "qmsckjxw"; 
const char* mqttPassword = "7FhpnddzzyqC"; 


WiFiClient espClient;

PubSubClient client(espClient);

/*
Portas nodemcu
-------------------------------------------------

NodeMCU / ESP8266  |  NodeMCU / ESP8266  |

D0 = 16            |  D6 = 12            |

D1 = 5             |  D7 = 13            |

D2 = 4             |  D8 = 15            |

D3 = 0             |  D9 = 3             |

D4 = 2             |  D10 = 1            |

D5 = 14            |                     |

-------------------------------------------------

*/


//Iniciação das variáveis
const int LED1 = 13;
const int LED2 = 12;

//Iniciação de variaveis para comunicacao MQTT
void mqtt_callback(char* topic, byte* dados_tcp, unsigned int length);


void setup() { 
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  
  Serial.begin(115200);

  WiFi.begin(ssid, password); 

  while (WiFi.status() != WL_CONNECTED) 
  {   
     delay(100);
    Serial.println("Conectando a WiFi..");
  }

  Serial.println("Conectado!"); 
  
//Inicia comunicação com Servidor MQTT
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);

 
//Mantem a conexão conexão com Servidor MQTT.
  while (!client.connected()) {
    Serial.println("Conectando ao servidor MQTT...");
    if (client.connect("Projeto", mqttUser, mqttPassword ))
    {
      Serial.println("Conectado ao servidor MQTT!"); 
    } else {
      Serial.print("Falha ao conectar ");
      Serial.print(client.state());
      delay(2000);
    }
  }
  
//Publica no Servido MQTT
  client.publish("Status ","Reiniciado!");
  client.publish("Placa","Em funcionamento!");

//Se inscreve em Tópicos no Servidor MQTT
  client.subscribe("LED"); 
}

//Metódo responsavel por ouvir Servidor MQTT 
void callback(char* topic, byte* dados_tcp, unsigned int length) {
    for (int i = 0; i < length; i++) {
      }
  if ((char)dados_tcp[0] == 'L' && (char)dados_tcp[1] == '1') {
    digitalWrite(LED1, HIGH);   
  } else if((char)dados_tcp[0] == 'D' && (char)dados_tcp[1] == '1'){
    digitalWrite(LED1, LOW);  

  }
  if ((char)dados_tcp[0] == 'L' && (char)dados_tcp[1] == '2') {
    digitalWrite(LED2, HIGH);   
  } else if((char)dados_tcp[0] == 'D' && (char)dados_tcp[1] == '2'){
    digitalWrite(LED2, LOW);  
  }
} 



void loop() {        
     client.loop();
 }
